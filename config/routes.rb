Rails.application.routes.draw do
  post "/sign", to: "users#create"
  post "/login", to: "users#login"

  resources :todos
end
