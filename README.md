# Todo in rails

Api to train the basics of Ruby on Rails. The api consists of:

* Register a user. Route: /sign Type: Post

* Login. Route: /login Type: Post

* Create a task for the user. Route: /todos Type: Post

* List user tasks. Route: /todos Type: Get

* Display a user-specific task. Route: /todos/:id Type: Get

* Update task status. Route: /todos/:id Type: Put

* Delete a user task. Route: /todos/:id Type: Delete


## Steps to run

* Clone the repository `git clone git@gitlab.com:luciano_junior_/todo-in-rails.git`

* Access the repository `cd todo-in-rails/`

* Install required gems with `bundle install`

* run the migrations `rails db:migrate`

* start the server `rails s`

