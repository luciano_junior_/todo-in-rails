class TodosController < ApplicationController
    before_action :authorize_request
    def index
      @todos = @user.todos.all
      render json: @todos, status: :ok
    end
  
    def show
      @todo = @user.todos.find(params[:id])
      render json: @todo, status: :ok
    end
  
    def create
      @todo = Todo.new(todo_params.merge(user: @user))
      if @todo.save
        render json: @todo, status: :created 
      else
        render json: @todo.errors, status: :unprocessable_entity
      end
    end
  
    def update
      @todo = @user.todos.find(params[:id])
  
      if @todo.update(todo_params)
        render json: @todo
      else
        render json: @todo.errors, status: :unprocessable_entity
      end
    end
  
    def destroy
      @todo = @user.todos.find(params[:id])
      @todo.destroy
    end
  
    private
    def todo_params
      params.require(:todo).permit(:task, :status)
    end
  end