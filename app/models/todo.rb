class Todo < ApplicationRecord
  belongs_to :user

  validates :task, presence: true

  VALID_STATUSES = ['done', 'in progress', 'pending']

  validates :status, inclusion: { in: VALID_STATUSES }
end